CONSIDERANDO A FALHA AO TRANSFORMAR O PROJETO EM ORIENTAÇÃO A OBJETOS, SEGUE TAMBÉM O JOGO SOMENTE EM UM ARQUIVO .CPP


->PARA COMPILAR:

	make game
->PARA RODAR O JOGO:

 	make play 



PARA COMPILAR O LABIRINTO DIGITE NO TERMINAL: 

	make 	->O makefile fará o serviço


PARA RODAR O JOGO: 

	make run	->O makefile fará o serviço


PARA JOGAR, UTILIZE AS SETAS DE DIREÇÃO:

->Up arrow (para ir para cima)

->Down arrow (para ir para baixo)

->Left arrow (para ir para a esquerda)

->Right arrow (para ir para a direita)

-> Digite a tecla "q" para sair do jogo

CONTEXTO DO JOGO:

#Você é um minerador atrás de ouro e deve chegar a ele no final do labirinto.


OBJETIVO DO JOGO:

#Chegar com seu player ao final do labirinto e alcançar o G (gold)




IMPLEMENTAÇÃO DO PROJETO:

Foi implementado um projeto que construísse um mapa, pelo qual o Player deveria passar e chegar ao seu destino (GOLD).

As funções foram implementadas a partir de tentativas, soluções e sugestões encontradas na internet.

Primeiramente, foi implementada um "Main" com o jogo funcionando, utilizando funções e procedimentos.

Depois disso, a tentativa de converter todo o projeto em Orientação a Objetos foi falha, pois haviam erros que não foi possível solucionar a tempo. Na hora da debugação, haviam diversos erros que não consegui encontrar uma alternativa, ao converter para OOP.

Fica então, o a tentativa do projeto em OOP, em C++. E também o projeto somente em um arquivo Main, como explicitado acima.




Diogo Oliveira Santana

12/0115581
