# Executavel
BINFOLDER := bin/
# .hpp
INCFOLDER := inc/
# .cpp
SRCFOLDER := src/
# .o
OBJFOLDER := obj/

CC := g++

NCURSESFLAGS:= -lm -I headers -lncurses

SRCFILES := $(wildcard src/*.cpp)

game: $(Labirinto.cpp) 
	$(CC) -o game Labirinto.cpp $(NCURSESFLAGS)

all: $(SRCFILES:src/%.cpp=obj/%.o)
	$(CC)  obj/*.o -o bin/prog $(NCURSESFLAGS)

play: game
	./game

obj/%.o: src/%.cpp
	$(CC)  -c $< -o $@ -I./inc $(NCURSESFLAGS)

run: bin/prog
	bin/prog

.PHONY: clean
clean:
	rm -rf obj/*
	rm -rf bin/*
	rm -rf Labirinto.o game
