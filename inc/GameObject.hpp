#ifndef GAME_OBJECT_H
#define GAME_OBJECT_H


#include<iostream>
#include<string>
#include <vector>
#include <cstdlib>
#include <utility>
#include <algorithm>
#include<ncurses.h>

using namespace std;

class GameObject
{
 protected:
	char sprite;
	int pos_x;
	int pos_y;

 public:

	GameObject();
	~GameObject();
	
	void setPosX();
	void setPosY();
	
	int getPosX();
	int getPosy();
};

#endif
