#ifndef DRAW_H
#define DRAW_H

#include<string>
#include <vector>
#include <cstdlib>
#include <utility>
#include <algorithm>
#include<ncurses.h>

using namespace std;



class Draw
{
 private:

 public:
	void display(vector<vector<char> >& m);
	void display_maze_with_attributes(const vector<vector<char> >& m, unsigned int attr);
	void build_maze(vector<vector<char> >& m);
	int random_n (int n);
	void dig_maze(vector<vector<char> >& m, int r, int c, char *wall);
	void build_maze(vector<vector<char> >& m);

};

#endif
