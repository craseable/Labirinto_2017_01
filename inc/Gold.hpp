#ifndef GOLD_H
#define GOLD_H

#include<iostream>
#include<string>
#include <vector>
#include <cstdlib>
#include <utility>
#include <algorithm>
#include<ncurses.h>

#include "GameObject.hpp"

using namespace std;

class Gold : public GameObject
{
 protected:
	int pos_x;
	int pos_y;
 public:

	
	Gold();
	~Gold();
	
	void setPosX(int pos_x);
	void setPosY(int pos_y);
	
	int getPosX();
	int getPosY();
	
	void inputGold(int pos_x,int pos_y, char gold,vector<vector<char> >& m);

};

#endif
