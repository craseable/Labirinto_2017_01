#ifndef PLAYER_H
#define PLAYER_H

#include<iostream>
#include<string>
#include <vector>
#include <cstdlib>
#include <utility>
#include <algorithm>
#include<ncurses.h>

#include "GameObject.hpp"

using namespace std;


class Player : public GameObject
{
 public:
	
	Player(char sprite,int pos_x,int pos_y);
	~Player();
	
	void setSprite(char sprite);	
	void setPosX(int pos_x);
	void setPosY(int pos_y);

	char getSprite();
	int getPosX();
	int getPosY();
	
	void print_gameover(vector<vector<char> >& m);
	void move(char sprite, int pos_x, int pos_y,vector<vector<char> >& m);	
};

#endif
