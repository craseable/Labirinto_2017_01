#include <vector>
#include <cstdlib>
#include <utility>
#include <iostream>
#include <algorithm>
using namespace std;

#include <ncurses.h>

const char MAZE_DUDE  = 'D';
const char MAZE_WALL  = '#';
const char MAZE_SPACE = ' ';
const char MAZE_TRACE = '.';
const char MAZE_GOLD  = 'G';
const char MAZE_MONEY = '$';

/*
 Mostra o labirinto na tela do terminal, printando cada caracterece usando a função mvaddch() da NCURSES
 */
void display_maze(const vector<vector<char> >& m)
{
    for(int row = 0; row < m.size(); row++) {
        for(int col = 0; col < m[0].size(); col++) {
            mvaddch(row, col, m[row][col]);
        }
    }
    refresh();
}

/*
 Mostra o labirinto no terminal, com os atributos dados
 Um exemplo de atributo pode ser A_BOLD e A_BLINK para fazer o caractere piscar e ficar em negrito
 */
void display_maze_with_attributes(
        const vector<vector<char> >& m, unsigned int attr)
{
    for(int row = 0; row < m.size(); row++) {
        for(int col = 0; col < m[0].size(); col++) {
            mvaddch(row, col, m[row][col] | attr);
        }
    }
    refresh();
}

/*
 Retorna um valor aleatório inteiro x, onde 0 <= x < n.
 */
int random_n (int n) { return random() % n; }


void dig_maze(vector<vector<char> >& m, int r, int c, char *wall) //r = row, c = column
{
    // Estou fora dos limites?
    if (r < 0 || c < 0 || r >= m.size() || c >= m[0].size())
        return;

    // Estou em uma parede, ou em uma célula pela qual eu já passei??
    if (m[r][c] == MAZE_WALL || m[r][c] == MAZE_SPACE)
        return;

    //O ponteiro wall aponta para a parede que pulamos entre as chamadas recursivas. Derruba ela
    // (wall == NULL para a sua primeira aparição.)
    if (wall) {
        *wall = MAZE_SPACE;
    }

    // Cavando esta célula
    m[r][c] = MAZE_SPACE;
    display_maze(m);

    // Decidir de forma randomica como exploro as direções
    // Norte, Sul, Leste ou Oeste. Uso random_shuffle() para sortear um array.
    char D[4] = { 'N', 'S', 'L', 'O' };
    random_shuffle(D, D+4, random_n);

    for (int i = 0; i < 4; i++) {

        // m[rr][cc] será a celula que tento cavar.
        int rr = r;
        int cc = c;
        char *wall;

        switch (D[i]) {
            case 'N':
                rr -= 2;
                wall = &m[r-1][c];
                break;
            case 'S':
                rr += 2;
                wall = &m[r+1][c];
                break;
            case 'O':
                cc -= 2;
                wall = &m[r][c-1];
                break;
            case 'L':
                cc += 2;
                wall = &m[r][c+1];
                break;
            default:
                cerr << "unknown direction" << endl;
                exit(1);
        }

        // Cavando as células recursivamente de [rr] e [cc]
        dig_maze(m, rr, cc, wall);
    }
}


void build_maze(vector<vector<char> >& m)
{
    // minRow,minCol,maxRow,maxCol indicam o tamanho máximo do labirinto.
    int minRow = 1;
    int minCol = 1;
    int maxRow = m.size()    - 2;
    int maxCol = m[0].size() - 2;

    // Junta o gerador de números pseudo-aleatórios com o tamanho da tela, para que eu possa ter o mesmo labirinto em um determinado tamanho de terminal
    srandom(maxRow * maxCol);

    /* Inicialize a matriz 'm' do labirinto em paredes e células alternadas
	
	      #########
	      #.#.#.#.# 
	      #########
	      #.#.#.#.# 
	      #########
    */
    for (int r = minRow; r <= maxRow; r += 2) {
        for (int c = minCol; c <= maxCol; c += 2) {
            m[r][c] = MAZE_TRACE;
        }
    }

    // Começa a cavar o labirinto recursivamente
    dig_maze(m, 1, 1, NULL);
}

//printa GAME OVER piscando
void print_gameover(const vector<vector<char> >& m)
{
    string s("[GAME OVER]");
    move(m.size()-1, (m[0].size()-1)/2 - (s.length()-1)/2);
    attron(A_REVERSE | A_BLINK);
    printw("%s", s.c_str());
    attroff(A_REVERSE | A_BLINK);
}

int main() 
{

    initscr(); // Coloca o terminal em modo de tela cheia
    cbreak(); // Desabilitar o buffer de teclado, para ler entradas "cruas"
    keypad(stdscr, TRUE); // Habilita o modo "keypad", para utilização das setas de direção do teclado
    noecho(); // Não mostra a entrada do usuário na tela
    curs_set(0); // Deixa o cursos invisível

    // Inicializa a matriz do Labirinto com dimensões de tela cheia

    // A initscr() configura LINES e COLS para o tamanho atual da tela do terminal
    vector<vector<char> > m(LINES, vector<char>(COLS, MAZE_WALL));

    // Constrói e mostra o Labirinto

    build_maze(m);
    display_maze(m);

    // Coloca o GOLD perto do canto direito inferior

    int goldRow = -1;
    int goldCol = -1;
    for (int row = m.size() - 2; row >= 1; --row) {
        for (int col = m[0].size() - 2; col >= 1; --col) {
            if (m[row][col] == MAZE_SPACE) {
                goldRow = row;
                goldCol = col;
                break;
            }
        }
        if (goldRow != -1) 
            break;
    }
    if (goldRow != -1) {
        mvaddch(goldRow, goldCol, MAZE_GOLD);
    } else {
        cerr << "Sem espaço para o Ouro, deixe sua tela maior." << endl;
        exit(1);
    }

    // O jogo começa

    // The dude start at the upper left corner.
    int row = 1;
    int col = 1;
    mvaddch(row, col, MAZE_DUDE);

    while (1) {
        int key = getch(); // Lê uma inserção no teclado.

        // Digite "q" para sair
        if (key == 'q') {
            break;
        }

        //As setas direcionais mexem o jogador, deixando um rastro na tela.
        if (key == KEY_UP || key == KEY_DOWN || 
                key == KEY_LEFT || key == KEY_RIGHT) {
            mvaddch(row, col, MAZE_TRACE);
            if (KEY_UP    == key && MAZE_SPACE == m[row - 1][col]) row--;
            if (KEY_DOWN  == key && MAZE_SPACE == m[row + 1][col]) row++;
            if (KEY_LEFT  == key && MAZE_SPACE == m[row][col - 1]) col--;
            if (KEY_RIGHT == key && MAZE_SPACE == m[row][col + 1]) col++;
            mvaddch(row, col, MAZE_DUDE);
            refresh(); // atualiza a tela do terminal.
        }

        // Se o player achou o ouro, GAME OVER
        if (row == goldRow && col == goldCol) {
            print_gameover(m);
            while ((key = getch()) != 'q') {}
            break;
        }
    }

    endwin();
}
