#include <vector>
#include <cstdlib>
#include <utility>
#include <iostream>
#include <algorithm>
using namespace std;

#include <ncurses.h>
#include "Status.hpp"

void Status :: print_gameover(const vector<vector<char> >& m) 
//printa GAME OVER piscando
{
	string s("[GAME OVER]");
	move(m.size()-1, (m[0].size()-1)/2 - (s.length()-1)/2);
	attron(A_REVERSE | A_BLINK);
	printw("%s", s.c_str());
	attroff(A_REVERSE | A_BLINK);
}
