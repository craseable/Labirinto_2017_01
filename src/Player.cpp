#include<iostream>
#include<string>
#include <vector>
#include <cstdlib>
#include <utility>
#include <algorithm>
#include<ncurses.h>
#include "Player.hpp"


using namespace std;



Player :: Player(char sprite,int pos_x,int pos_y)
{
	this -> pos_x = pos_x;
	this -> pos_y = pos_y;
	this -> sprite = sprite;
}

Player :: ~Player(){}

void Player :: setSprite(char sprite)
{
	this -> sprite = sprite;
}

void Player :: setPosX(int pos_x)
{
	this -> pos_x = pos_x;
}

void Player :: setPosY(int pos_y)
{
	this -> pos_y = pos_y;
}

char Player :: getSprite()
{
	return sprite;
}

int Player :: getPosX()
{
	return pos_x;
}

int Player :: getPosY()
{
	return pos_y;
}

void Player :: print_gameover(const vector<vector<char> >& m)
//printa GAME OVER piscando
{
        cbreak(); // desabilitao buffer de linhas, para pegar inputs "crus"
        keypad(stdscr, TRUE); // habilita o modo "keypad" para utilizar as setas

	
	string s("[GAME OVER]");
        move(m.size()-1, (m[0].size()-1)/2 - (s.length()-1)/2);
        attron(A_REVERSE | A_BLINK);
        printw("%s", s.c_str());
        attroff(A_REVERSE | A_BLINK);
}


void Player :: move(char sprite, int pos_x, int pos_y, vector<vector<char> >& m)
{
	cbreak(); // desabilitao buffer de linhas, para pegar inputs "crus"
	keypad(stdscr, TRUE); // habilita o modo "keypad" para utilizar as setas
	mvaddch(pos_x,pos_y,sprite);
	
	Player * tmp = new Player('@',1,1);
	Gold * gold = new Gold();
	gold -> inputGold('G',-1,-1,m);
	while (1) 
	{
		int key = getch(); // Lê uma inserção no teclado.
		char track = '.';
		// Digite "q" para sair
		if (key == 'q' || key == 'Q') {
			break;
		}

		// As setas direcionais mexem o jogador, deixando um rastro na tela.
		if (key == KEY_UP || key == KEY_DOWN || 
		key == KEY_LEFT || key == KEY_RIGHT) {
			mvaddch(pos_x, pos_y, track);
			if (KEY_UP    == key && ' '  == m[pos_x - 1][pos_y]) pos_x--;
			if (KEY_DOWN  == key && ' ' == m[pos_x + 1][pos_y]) pos_x++;
			if (KEY_LEFT  == key && ' ' == m[pos_x][pos_y - 1]) pos_y--;
			if (KEY_RIGHT == key && ' ' == m[pos_x][pos_y + 1]) pos_y++;
			mvaddch(pos_x, pos_y, sprite);
        		refresh(); // atualiza a tela do terminal.
	
		// Se o player achou o ouro, GAME OVER
        	if (pos_x == (gold->getPosX()) && pos_y == (gold->getPosY())) {
            	tmp -> print_gameover(m);
            	while ((key = getch()) != 'q' && (key = getch()) != 'Q') {}
            	break;
        	}
        	}
	}
}
