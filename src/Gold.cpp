#include<iostream>
#include<string>
#include <vector>
#include <cstdlib>
#include <utility>
#include <algorithm>
#include<ncurses.h>

#include "Gold.hpp"



using namespace std;


/*Gold :: Gold(char gold, int pos_x, int pos_y)
{
	this -> gold = gold;
	this -> pos_x = pos_x;
	this -> pos_y = pos_y;	
}*/
Gold :: Gold(){}
Gold :: ~Gold(){}

void setPosX(int pos_x)
{
	this -> pos_x = pos_x;
}
	
void setPosY(int pos_y)
{
	this -> pos_y = pos_y;
}

int getPosX()
{
	return pos_x;
}

int getPosY()
{
	return pos_y;
}

void Gold :: inputGold(int pos_x,int pos_y, char gold,vector<vector<char> >& m)
{
	for (int linha = m.size() - 2; linha >= 1; --linha) 
	{
		for (int coluna = m[0].size() - 2; coluna >= 1; --coluna) 
		{
        		if (m[linha][coluna] == ' ') {
        		        pos_x = linha;
                		pos_y = coluna;
               	 		break;
            		}
        	}
        	if (pos_x != -1) 
        		break;
    	}
    	if (pos_x != -1) 
	{
    		mvaddch(pos_x, pos_y, gold);
    	} else 
	{
        	cerr << "Sem espaço para o Ouro, deixe sua tela maior." << endl;
        	exit(1);
    	}

}
