#include<iostream>
#include <vector>
#include <cstdlib>
#include <utility>
#include <algorithm>
#include <ncurses.h>

#include "Draw.hpp"

using namespace std;

void Draw :: display(vector<vector<char> >& m)
{
	for(int row = 0; row < m.size(); row++)
	{
        	for(int col = 0; col < m[0].size(); col++) 
		{
       	     		mvaddch(row, col, m[row][col]);
        	}
    	}
	
	//Ncurses não printa nada até que refresh() seja chamada
	refresh();
}

void Draw :: display_maze_with_attributes(const vector<vector<char> >& m, unsigned int attr)
{
	for(int row = 0; row < m.size(); row++) 
	{
	        for(int col = 0; col < m[0].size(); col++) 
		{
        		mvaddch(row, col, m[row][col] | attr);
        	}
	}
	
	//Ncurses não printa nada até que refresh() seja chamada

	refresh();
}



int Draw :: random_n(int n)
{
	return random() % n;
}

void Draw :: dig_maze(vector<vector<char> >& m, int r, int c, char *wall) //r = row ; c = column
{
	// Estou fora dos limites?
	if (r < 0 || c < 0 || r >= m.size() || c >= m[0].size())
	{
        	return;
	}

	// Estou em uma parede, ou em uma célula pela qual eu já passei??
	if (m[r][c] == '#' || m[r][c] == ' ')
	{
        	return;
	}

	//O ponteiro wall aponta para a parede que pulamos entre as chamadas recursivas. Derruba ela
	// (wall == NULL para a sua primeira aparição.)
	if (wall) 
	{
	        *wall = ' ';
	}

	// Cavando esta célula
	m[r][c] = ' ';
	display_maze(m);

	// Decidir de forma randomica como exploro as direções
	// Norte, Sul, Leste ou Oeste. Uso random_shuffle() para sortear um array.
	char D[4] = { 'N', 'S', 'L', 'O' };
	random_shuffle(D, D+4, random_n);

	for (int i = 0; i < 4; i++) 
	{

		// m[rr][cc] será a celula que tento cavar.
	        int rr = r;
        	int cc = c;
        	char *wall;

        	switch (D[i]) {
           	case 'N':
                	rr -= 2;
                	wall = &m[r-1][c];
                	break;
            	case 'S':
                	rr += 2;
                	wall = &m[r+1][c];
                	break;
            	case 'O':
                	cc -= 2;
               		wall = &m[r][c-1];
                	break;
            	case 'L':
               		cc += 2;
                	wall = &m[r][c+1];
                	break;
            	default:
                	cerr << "Direção Desconhecida." << endl;
                	exit(1);
        	}

        	//Cavando as células recursivamente de [rr] e [cc].
        	dig_maze(m, rr, cc, wall);
    	}

}


void Draw :: build_maze(vector<vector<char> >& m)
{
	// minRow,minCol,maxRow,maxCol indicam o tamanho máximo do labirinto.
	int minRow = 1;
	int minCol = 1;
	int maxRow = m.size()    - 2;
	int maxCol = m[0].size() - 2;

	// Junta o gerador de números pseudo-aleatórios com o tamanho da tela, para que eu possa ter o mesmo labirinto em um determinado tamanho de terminal
	srandom(maxRow * maxCol);

	/* Inicialize a matriz 'm' do labirinto em paredes e células alternadas
	
	      #########
	      #.#.#.#.# 
	      #########
	      #.#.#.#.# 
	      #########
	*/
	for (int r = minRow; r <= maxRow; r += 2) 
	{
        	for (int c = minCol; c <= maxCol; c += 2) 
		{
        		m[r][c] = '-';
        	}
    	}

	// Começa a cavar o labirinto recursivamente.
	dig_maze(m, 1, 1, NULL);
}




