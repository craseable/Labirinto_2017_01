#include<iostream>
#include<string>
#include <vector>
#include <cstdlib>
#include <utility>
#include <algorithm>
#include<ncurses.h>

#include "GameObject.hpp"
#include "Player.hpp"
#include "Status.hpp"
#include "Draw.hpp"
#include "Gold.hpp"

using namespace std;

int main()
{
	

	// Initialize the terminal screen using Ncurses init routines.
	initscr(); // switch terminal screen to fullscreen curses mode
	cbreak(); // disable line buffering so that we get raw keystrokes
	keypad(stdscr, TRUE); // enable keypad mode (for arrow and fn keys)
	noecho(); // do not print the user's keystrokes to the screen
	curs_set(0); // make the cursor invisible
	
	// Initialize the maze matrix with the full screen dimemsions.
	// - the Ncurses libarary's initscr() sets LINES and COLS to  
	//   the current terminal screen size.

    	vector<vector<char> > m(LINES, vector<char>(COLS, '#'));
	

	//Instanciando os objetos
	Player * player = new Player('@',1,1);
	Draw * map = new Draw();
	Gold * gold = new Gold();
	Status * status = new Status();
	
	//Construindo o mapa
	map -> build_maze(m);
	map -> display(m);
	

	//Colocando o ouro no mapa
	gold -> inputGold(-1,-1,'G');
	

	//Iniciando o jogo
	player -> move('@',1,1,m);
	

	// All done; get out of the fullscreen curses mode.
	endwin();
	


	return 0;
}
